<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>

<html>
	<head>
		<title>no Page</title>
	</head>
	<body>
		존재하지 않는 페이지 입니다.
		<br>
		<br>
		<a href = "<c:url value="/category"/>">카테고리 페이지로 돌아가기</a>
	</body>
</html>