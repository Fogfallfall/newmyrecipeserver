<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<title>menu</title>
	</head>
	
	<body>
	
	<h1>
		Menu
	</h1>
	
		<table border="1" id = "menutable">
			<tr align="center">
				<td width="100px"> ID </td>
				<td> 카테고리 아이디 </td>
				<td width="100px"> 메뉴 이름 </td>
			</tr>
		
			<c:forEach var="menu" items="${menuList}">
				<tr align="center">
					<td>
						<input type ="radio" name="menu_id" value =${menu.id}>
						${menu.id}
					</td>
					<td id ="categoryId">${menu.category_id}</td>
					<td>${menu.menu_name}</td>
				</tr>
			</c:forEach>
		</table>
		<input type = "button" value= "편집" onClick="showMenu();"/>
		<script>
			function showMenu()
			{
				var obj = document.getElementsByName("menu_id");
				var checked_value = " ";
				
				for (i = 0; i <obj.length; i++)
				{
					if(obj[i].checked)
					{
						checked_value = obj[i].value;
					}
				}
				var loc = "/newmyrecipe/category/"+${cateId}+"/menu/" + checked_value;
				location.href = loc;
			}
		</script>
		<a href = "<c:url value="/category/${cateId}"/>">돌아가기</a>
		<br>
		<br>
		<form action="<c:url value="/category/${cateId}/menu"/>" method="POST">
			<input type ="hidden" name="category_id" value=${cateId}>
			<table border = "1">
				<tr align = "center">
					<td>메뉴 이름</td>
				</tr>
				<tr>
					<td><input name = "menu_name"></td>
				</tr>
			</table>
			<div>
				<input type="submit" value="추가">
			</div>
		</form>
	</body>
</html>