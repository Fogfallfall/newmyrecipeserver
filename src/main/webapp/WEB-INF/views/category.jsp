<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<style type="text/css">
 		form{display:inline}
	</style>
	
	<head>
		<title>category</title>
	</head>
	<body>
	
	<h1>
		${category.category_name}
	</h1>
	
	<form method = "POST" action = "<c:url value = "/category/${category.id}"/>">
		<input type ="hidden" name ="_method" value="PUT">
		 
		<table border = "1">
			<tr align = center>
				<td>아이디</td>
				<td>카테고리 이름</td>
			</tr>
			<tr align = center>
				<td>${category.id}</td>
				<td><input type = "text" name="category_name" value = ${category.category_name}></td>
			</tr>
		</table>
		<button type="submit">수정</button>
	</form>
		<form method = "POST" action="<c:url value = "/category/${category.id}"/>">
			<input type="hidden" name="_method" value="DELETE">
			<button type="submit">삭제</button>
		</form>
		<br>
		<br>
		<a href = "<c:url value="/category/${category.id}/menu"/>">메뉴이동</a>
		<a href = "<c:url value="/category"/>">돌아가기</a>
		
	</body>
</html>