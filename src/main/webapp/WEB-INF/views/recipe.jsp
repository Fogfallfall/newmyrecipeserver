<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>recipe</title>
	</head>
	
	<body>
		
		<h1>
			Recipe
		</h1>
		
		<form method="POST" action = "<c:url value = "/category/${cateId}/menu/${menuId}/recipe"/>">
			<input type = "hidden" name="_method" value="PUT">
			<table border = "1">
				<tr align = center>
					<td>메뉴 아이디</td>
					<td>레시피</td>
				</tr>
				<tr align = center>
					<td>${menuId}</td>
					<td><textarea rows="15" cols="30" name="recipe">${recipe}</textarea></td>				
				</tr>
			</table>
			<button type="submit">수정</button>
		</form>	
		<a href = "<c:url value = "/category/${cateId}/menu/${menuId}"/>">돌아가기</a>
	</body>
</html>