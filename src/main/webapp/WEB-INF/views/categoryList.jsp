<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Categories</title>
	</head>
	<body>
		<h1>
			Categories
		</h1>
	
		<table border="1">
			<tr align="center">
				<td width="100px"> 아이디 </td>
				<td width="100px"> 카테고리 이름 </td>
			</tr>
	
			<c:forEach var="category" items="${categoryList}">
				<tr align="center">
					<td>
						<input type ="radio" name="category_id" value =${category.id}>
						${category.id}
					</td>
					<td>${category.category_name}</td>
				</tr>
			</c:forEach>
		</table>
	
		<input type="button" value = "편집" onclick = "ShowCategory();"/>
	
		<script>
			function ShowCategory()
			{
				var obj = document.getElementsByName("category_id");
				var checked_value = " ";
				
				for (i = 0; i <obj.length; i++)
				{
					if(obj[i].checked)
					{
						checked_value = obj[i].value;
					}
				}
				var loc = "/newmyrecipe/category/" + checked_value;
				location.href = loc;
			}
		</script>
		
		<br>
		<br>
		
		<form action="<c:url value="/category"/>" method="POST">
			<table border = "1">
				<tr align = "center">
					<td>카테고리 이름</td>
				</tr>
				<tr>
					<td><input name = "category_name"></td>
				</tr>
			</table>
			<div>
				<input type="submit" value="추가">
			</div>
		</form>
		
	</body>
</html>