<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<style type="text/css">
 		form{display:inline}
	</style>
	
	<head>
		<title>menu</title>
	</head>
	<body>
	
	<h1>
		${menu.menu_name}
	</h1>
		<form method = "POST" action="<c:url value="/category/${menu.category_id}/menu/${menu.id}"/>">
			<input type="hidden" name="_method" value="PUT">
			<table border = "1">
				<tr align = center>
					<td>아이디</td>
					<td>카테고리 아이디</td>
					<td>메뉴 이름</td>
				</tr>
				<tr align = center>
					<td>${menu.id}</td>
					<td>${menu.category_id}
					<td><input type = "text" name="menu_name" value = ${menu.menu_name}></td>
				</tr>
			</table>
			<button type="submit">수정</button>
		</form>
		<form method = "POST" action="<c:url value = "/category/${menu.category_id}/menu/${menu.id}"/>">
			<input type="hidden" name="_method" value="DELETE">
			<button type="submit">삭제</button>
		</form>
		<br>
		<br>
		<a href = "<c:url value = "/category/${menu.category_id}/menu/${menu.id}/recipe"/>">레시피 보기</a>
		<a href = "<c:url value = "/category/${menu.category_id}/menu"/>">돌아가기</a>
	</body>
</html>