package com.jinhyun.newmyrecipe.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() 
	{
		
		return "redirect:/auth";
	}
	
	
	@RequestMapping(value="/auth", method = RequestMethod.GET)
	public String admin()
	{
		return "auth";
	}
	
	@RequestMapping(value="/auth", method = RequestMethod.POST)
	public String adminOk(HttpSession session, boolean auth)
	{
		session.setAttribute("auth", auth);
		System.out.println(auth);
		return "redirect:/category";
	}
}
