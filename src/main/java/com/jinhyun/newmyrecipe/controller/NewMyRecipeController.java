package com.jinhyun.newmyrecipe.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jinhyun.newmyrecipe.spring.Categories;
import com.jinhyun.newmyrecipe.spring.Menu;
import com.jinhyun.newmyrecipe.spring.Recipes;
import com.jinhyun.newmyrecipe.spring.NewMyRecipeService;

@Controller
public class NewMyRecipeController
{	
	private NewMyRecipeService newMyRecipeService;
	private String jsonString = "";
	
	@Autowired
	public NewMyRecipeController(NewMyRecipeService newMyRecipeService)
	{
		this.newMyRecipeService = newMyRecipeService;
	}
	
	ObjectMapper mapper = new ObjectMapper();
	
	@RequestMapping(value="/category", method=RequestMethod.GET)
	public String list(Model model, HttpSession session)
	{	
		System.out.println((Boolean)session.getAttribute("auth"));
		
		if(session.getAttribute("auth") != null)
		{
			model.addAttribute("categoryList", newMyRecipeService.selectAllCategories());				
			return "categoryList";
		}
		else
		{
			model.addAttribute("msg", "관리자 권한이 필요합니다");
			model.addAttribute("url", "/newmyrecipe/auth");
			return "noAdmin";
		}
	}
	
	@RequestMapping(value="/category", method=RequestMethod.POST)
	public String write(Model model, String category_name, HttpSession session)
	{
		if(session.getAttribute("auth") != null)
		{
			newMyRecipeService.insertCategories(category_name);
			return "redirect:/category";
		}
		else
		{
			model.addAttribute("msg", "관리자 권한이 필요합니다");
			model.addAttribute("url", "/newmyrecipe/auth");
			return "noAdmin";
		}
	}
	
	@RequestMapping(value="/category/{id}", method=RequestMethod.GET)
	public String oneCategory(Model model, @PathVariable int id, HttpSession session)
	{
		if(session.getAttribute("auth") != null)
		{			
			if(newMyRecipeService.selectCategory(id).getId() != id)
			{
				return "noPage";
			}
			else
			{
				model.addAttribute("category", newMyRecipeService.selectCategory(id));
				return "category";
			}
		}
		else
		{
			model.addAttribute("msg", "관리자 권한이 필요합니다");
			model.addAttribute("url", "/newmyrecipe/auth");
			return "noAdmin";
		}
	}
	
	@RequestMapping(value="/category/{id}", method=RequestMethod.PUT)
	public String editCategory(Model model, @PathVariable int id, String category_name, HttpSession session)
	{
		if(session.getAttribute("auth") != null)
		{
			Categories categories = new Categories(id,category_name);
			newMyRecipeService.updateCategories(categories);
			return "redirect:/category/{id}";
		}
		else
		{
			model.addAttribute("msg", "관리자 권한이 필요합니다");
			model.addAttribute("url", "/newmyrecipe/auth");
			return "noAdmin";
		}
	}
	
	@RequestMapping(value="/category/{id}", method=RequestMethod.DELETE)
	public String delete(Model model, @PathVariable int id, HttpSession session)
	{
		if(session.getAttribute("auth") != null)
		{
			newMyRecipeService.deleteCategories(id);
			return "redirect:/category";
		}
		else
		{
			model.addAttribute("msg", "관리자 권한이 필요합니다");
			model.addAttribute("url", "/newmyrecipe/auth");
			return "noAdmin";
		}
	}
	
	@RequestMapping(value="/category/{category_id}/menu", method=RequestMethod.GET)
	public String menulist(Model model, @PathVariable int category_id, HttpSession session)
	{
		if(session.getAttribute("auth") != null)
		{
			if(newMyRecipeService.selectMenu(category_id).isEmpty() == true)
			{
				return "noPage";
			}
			else
			{
				model.addAttribute("cateId", category_id);
				model.addAttribute("menuList", newMyRecipeService.selectMenu(category_id));
				return "menuList";
			}
		}
		else
		{
			model.addAttribute("msg", "관리자 권한이 필요합니다");
			model.addAttribute("url", "/newmyrecipe/auth");
			return "noAdmin";
		}
	}
	
	@RequestMapping(value="/category/{category_id}/menu", method=RequestMethod.POST)
	public String menuInsert(Model model, @PathVariable int category_id, String menu_name, HttpSession session)
	{
		if(session.getAttribute("auth") != null)
		{
			int menu_id;
			String recipe = "";
				
			newMyRecipeService.insertMenu(category_id, menu_name);
		
			menu_id = newMyRecipeService.maxMenuId();
			Recipes recipes = new Recipes(menu_id, recipe);
		
			newMyRecipeService.insertRecipe(recipes);
		
			return "redirect:/category/{category_id}/menu";
		}
		else
		{
			model.addAttribute("msg", "관리자 권한이 필요합니다");
			model.addAttribute("url", "/newmyrecipe/auth");
			return "noAdmin";
		}
	}
	
	@RequestMapping(value="/category/{category_id}/menu/{id}", method=RequestMethod.GET)
	public String oneMenu(Model model, @PathVariable int category_id, @PathVariable int id, HttpSession session)
	{
		if(session.getAttribute("auth") != null)
		{
			if(newMyRecipeService.oneMenu(id).getCategory_id() != category_id || newMyRecipeService.oneMenu(id).getId() != id)
			{
				return "noPage";
			}
			else
			{
				model.addAttribute("menu", newMyRecipeService.oneMenu(id));
				return "menu";
			}
		}
		else
		{
			model.addAttribute("msg", "관리자 권한이 필요합니다");
			model.addAttribute("url", "/newmyrecipe/auth");
			return "noAdmin";
		}
	}
	
	@RequestMapping(value="/category/{category_id}/menu/{id}", method=RequestMethod.PUT)
	public String editMenu(Model model, @PathVariable int id, @PathVariable int category_id, String menu_name, HttpSession session)
	{
		if(session.getAttribute("auth") != null)
		{
			Menu menu = new Menu(id, category_id, menu_name);
			newMyRecipeService.updateMenu(menu);
			return "redirect:/category/{category_id}/menu/{id}";
		}
		else
		{
			model.addAttribute("msg", "관리자 권한이 필요합니다");
			model.addAttribute("url", "/newmyrecipe/auth");
			return "noAdmin";
		}
	}
	
	@RequestMapping(value="/category/{category_id}/menu/{id}", method=RequestMethod.DELETE)
	public String deleteMenu(Model model, @PathVariable int id, HttpSession session)
	{
		if(session.getAttribute("auth") != null)
		{
			newMyRecipeService.deleteMenu(id);
			return "redirect:/category/{category_id}/menu";
		}
		else
		{
			model.addAttribute("msg", "관리자 권한이 필요합니다");
			model.addAttribute("url", "/newmyrecipe/auth");
			return "noAdmin";
		}
	}
	
	@RequestMapping(value="/category/{category_id}/menu/{menu_id}/recipe", method=RequestMethod.GET)
	public String showRecipe(Model model, @PathVariable int category_id, @PathVariable int menu_id, HttpSession session)
	{
		if(session.getAttribute("auth") != null)
		{
			if(newMyRecipeService.oneMenu(menu_id).getCategory_id() != category_id || newMyRecipeService.oneMenu(menu_id).getId() != menu_id)
			{
				return "noPage";
			}
			else
			{
				model.addAttribute("cateId", category_id);
				model.addAttribute("menuId", menu_id);
				model.addAttribute("recipe", newMyRecipeService.selectRecipe(menu_id));
				return "recipe";
			}
		}
		else
		{
			model.addAttribute("msg", "관리자 권한이 필요합니다");
			model.addAttribute("url", "/newmyrecipe/auth");
			return "noAdmin";
		}
	}
	
	@RequestMapping(value="/category/{category_id}/menu/{menu_id}/recipe", method=RequestMethod.PUT)
	public String updateRecipe(@PathVariable int menu_id, String recipe)
	{
		Recipes recipes = new Recipes(menu_id, recipe);
		newMyRecipeService.updateRecipe(recipes);
		return "redirect:/category/{category_id}/menu/{menu_id}/recipe";
	}

	
	@RequestMapping(value="/categories", method=RequestMethod.GET)
	public @ResponseBody String CategoryJSON()
	{
		List<Categories> cate = newMyRecipeService.selectAllCategories();
		try 
		{
			jsonString = mapper.writeValueAsString(cate);
			System.out.println(jsonString);

		} 
		catch (JsonProcessingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonString;
	}
	
	@RequestMapping(value="categories/{category_id}/menu", method=RequestMethod.GET)
	public @ResponseBody String MenuJson(@PathVariable int category_id)
	{
		List<Menu> menu = newMyRecipeService.selectMenu(category_id);
		
		try
		{
			jsonString = mapper.writeValueAsString(menu);
			System.out.println(jsonString);
		}
		catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}
		return jsonString;
	}
	@RequestMapping(value="categories/menu/{menu_id}/recipe", method=RequestMethod.GET, produces="text/plain;charset=UTF-8")
	public @ResponseBody String RecipeJson(@PathVariable int menu_id)
	{
		String recipe = newMyRecipeService.selectRecipe(menu_id);
		
		try
		{
			jsonString = mapper.writeValueAsString(recipe);
			System.out.println(jsonString);
		}
		catch(JsonProcessingException e)
		{
			e.printStackTrace();
		}
		return jsonString;
	}
}