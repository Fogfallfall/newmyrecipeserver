package com.jinhyun.newmyrecipe.spring;

import java.util.List;

public interface MenuDao
{
	public List<Menu> selectMenu(int category_id);
	public Menu oneMenu(int id);
	public int maxMenuId();
	public void insert(int category_id, String menu_name);
	public void update(Menu menu);
	public void delete(int id);
}