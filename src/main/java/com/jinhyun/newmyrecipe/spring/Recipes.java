package com.jinhyun.newmyrecipe.spring;

public class Recipes
{
	private int menu_id;
	private String recipe;
	
	public Recipes(int menu_id, String recipe)
	{
		this.menu_id = menu_id;
		this.recipe = recipe;
	}
	
	public Recipes() {}
	
	public void setMenu_id(int menu_id)
	{
		this.menu_id = menu_id;
	}
	
	public void setRecipe(String recipe)
	{
		this.recipe = recipe;
	}
	
	public int getMenu_id()
	{
		return menu_id;
	}
	public String getRecipe()
	{
		return recipe;
	}	
}