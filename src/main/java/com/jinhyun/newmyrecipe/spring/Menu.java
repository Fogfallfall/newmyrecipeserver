package com.jinhyun.newmyrecipe.spring;

public class Menu
{
	private int id;
	private int category_id;
	private String menu_name;
	
	public Menu(int id, int category_id, String menu_name)
	{
		this.id = id;
		this.category_id = category_id;
		this.menu_name = menu_name;
	}
	
	public Menu () {}
	
	public void setId(int id)
	{
		this.id = id;
	}
	public void setCategory_id(int category_id)
	{
		this.category_id=category_id;
	}
	public void setMenu_name(String menu_name)
	{
		this.menu_name = menu_name;
	}
	
	public int getId()
	{
		return id;
	}
	public int getCategory_id()
	{
		return category_id;
	}
	public String getMenu_name()
	{
		return menu_name;
	}
}