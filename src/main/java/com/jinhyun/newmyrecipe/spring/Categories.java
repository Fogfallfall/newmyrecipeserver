package com.jinhyun.newmyrecipe.spring;

public class Categories
{
	private int id;
	private String category_name;
	
	public Categories(int id, String category_name)
	{
		this.id = id;
		this.category_name = category_name;
	}
	
	public Categories() {}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public void setCategory_name(String category_name)
	{
		this.category_name = category_name;
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getCategory_name()
	{
		return category_name;
	}
}
