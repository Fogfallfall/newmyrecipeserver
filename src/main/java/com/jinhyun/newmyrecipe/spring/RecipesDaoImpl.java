package com.jinhyun.newmyrecipe.spring;

import org.mybatis.spring.SqlSessionTemplate;

public class RecipesDaoImpl implements RecipesDao
{
	private SqlSessionTemplate sqlSessionTemplate;
	
	public RecipesDaoImpl(SqlSessionTemplate sqlSessionTemplate)
	{
		this.sqlSessionTemplate = sqlSessionTemplate;
	}

	@Override
	public String selectRecipe(int menu_id) 
	{		
		return sqlSessionTemplate.selectOne("selectRecipe",menu_id);
	}

	@Override
	public void insertRecipe(Recipes recipes) 
	{
		sqlSessionTemplate.insert("insertRecipe",recipes);
	}

	@Override
	public void updateRecipe(Recipes recipes) 
	{
		sqlSessionTemplate.update("updateRecipe",recipes);
	}

	@Override
	public void delete(int menu_id) 
	{
		sqlSessionTemplate.delete("delete",menu_id);
	}

}
