package com.jinhyun.newmyrecipe.spring;

import java.util.List;
import com.jinhyun.newmyrecipe.spring.Categories;

public interface CategoriesService
{
	public List<Categories> selectAll();
	public Categories selectCategory(int id);
	public int count();
	public void insert(String category_name);
	public void update(Categories categories);
	public void delete(int id);
}