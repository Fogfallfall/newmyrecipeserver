package com.jinhyun.newmyrecipe.spring;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

public class CategoriesDaoImpl implements CategoriesDao
{
	private SqlSessionTemplate sqlSessionTemplate;
	
	public CategoriesDaoImpl(SqlSessionTemplate sqlSessionTemplate)
	{
		this.sqlSessionTemplate = sqlSessionTemplate;
	}

	@Override
	public List<Categories> selectAll() 
	{
		return sqlSessionTemplate.selectList("selectAll");
	}
	
	@Override
	public Categories selectCategory(int id)
	{
		return sqlSessionTemplate.selectOne("selectCategory", id);
	}

	@Override
	public int count() 
	{
		return sqlSessionTemplate.selectOne("countCategory");
	}

	@Override
	public void insert(String category_name) 
	{
		sqlSessionTemplate.insert("insertCategory", category_name);
	}

	@Override
	public void update(Categories categories) 
	{
		sqlSessionTemplate.update("changeCategoryName", categories);
	}

	@Override
	public void delete(int id) 
	{
		sqlSessionTemplate.delete("deleteCategory", id);
	}
	
}