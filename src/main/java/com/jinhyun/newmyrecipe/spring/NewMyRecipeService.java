package com.jinhyun.newmyrecipe.spring;

import java.util.List;

import com.jinhyun.newmyrecipe.spring.Categories;
import com.jinhyun.newmyrecipe.spring.Menu;
import com.jinhyun.newmyrecipe.spring.Recipes;

public interface NewMyRecipeService
{
	//categories 기능
	public List<Categories> selectAllCategories();
	public Categories selectCategory(int id);
	public int countCategories();
	public void insertCategories(String category_name);
	public void updateCategories(Categories categories);
	public void deleteCategories(int id);
	
	//menu기능
	public List<Menu> selectMenu(int category_id);
	public Menu oneMenu (int id);
	public int maxMenuId();
	public void insertMenu(int category_id, String menu_name);
	public void updateMenu(Menu menu);
	public void deleteMenu(int id);
	
	//recipes 기능
	public String selectRecipe(int id);
	public void insertRecipe(Recipes recipes);
	public void updateRecipe(Recipes recipes);
	public void deleteRecipe(int menu_id);
}