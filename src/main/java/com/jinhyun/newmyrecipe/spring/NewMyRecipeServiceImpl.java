package com.jinhyun.newmyrecipe.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jinhyun.newmyrecipe.spring.CategoriesDao;
import com.jinhyun.newmyrecipe.spring.MenuDao;
import com.jinhyun.newmyrecipe.spring.RecipesDao;

@Service
public class NewMyRecipeServiceImpl implements NewMyRecipeService
{
	private CategoriesDao categoriesDao;
	private MenuDao menuDao;
	private RecipesDao recipesDao;
	
	@Autowired
	public NewMyRecipeServiceImpl(CategoriesDao categoriesDao, MenuDao menuDao, RecipesDao recipesDao)
	{
		this.categoriesDao = categoriesDao;
		this.menuDao = menuDao;
		this.recipesDao = recipesDao;
	}
	public NewMyRecipeServiceImpl() {}

	@Override
	public List<Categories> selectAllCategories() 
	{		
		return categoriesDao.selectAll();
	}
	
	@Override
	public Categories selectCategory(int id)
	{
		return categoriesDao.selectCategory(id);
	}

	@Override
	public int countCategories() 
	{
		return categoriesDao.count();
	}

	@Override
	public void insertCategories(String category_name) 
	{
		categoriesDao.insert(category_name);
	}

	@Override
	public void updateCategories(Categories categories) 
	{
		categoriesDao.update(categories);	
	}

	@Override
	public void deleteCategories(int id) 
	{
		categoriesDao.delete(id);
		
	}

	@Override
	public List<Menu> selectMenu(int category_id) 
	{
		return menuDao.selectMenu(category_id);
	}
	
	@Override
	public Menu oneMenu(int id)
	{
		return menuDao.oneMenu(id);
	}
	
	@Override
	public int maxMenuId() 
	{
		return menuDao.maxMenuId();
	}

	@Override
	public void insertMenu(int category_id, String menu_name) 
	{
		menuDao.insert(category_id, menu_name);
		
	}

	@Override
	public void updateMenu(Menu menu) 
	{
		menuDao.update(menu);
	}

	@Override
	public void deleteMenu(int id) 
	{
		menuDao.delete(id);
	}

	@Override
	public String selectRecipe(int menu_id) 
	{
		return recipesDao.selectRecipe(menu_id);
	}

	@Override
	public void insertRecipe(Recipes recipes) 
	{
		recipesDao.insertRecipe(recipes);
	}

	@Override
	public void updateRecipe(Recipes recipes) 
	{
		recipesDao.updateRecipe(recipes);
	}

	@Override
	public void deleteRecipe(int menu_id) 
	{
		recipesDao.delete(menu_id);
	}
	
}