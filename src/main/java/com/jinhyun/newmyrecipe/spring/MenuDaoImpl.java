package com.jinhyun.newmyrecipe.spring;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

public class MenuDaoImpl implements MenuDao
{
	private SqlSessionTemplate sqlSessionTemplate;
	
	public MenuDaoImpl(SqlSessionTemplate sqlSessionTemplate)
	{
		this.sqlSessionTemplate = sqlSessionTemplate;
	}

	@Override
	public List<Menu> selectMenu(int category_id) 
	{
		return sqlSessionTemplate.selectList("selectMenu", category_id);
	}
	
	@Override
	public Menu oneMenu(int id)
	{
		return sqlSessionTemplate.selectOne("oneMenu", id);
	}

	@Override
	public int maxMenuId() 
	{
		return sqlSessionTemplate.selectOne("MaxMenuId");
	}

	@Override
	public void insert(int category_id, String menu_name) 
	{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("category_id", category_id);
		map.put("menu_name", menu_name);
		
		sqlSessionTemplate.insert("insertMenu", map);
	}

	@Override
	public void update(Menu menu) 
	{
		sqlSessionTemplate.update("updateMenu", menu);
	}

	@Override
	public void delete(int id) 
	{
		sqlSessionTemplate.delete("deleteMenu", id);	
	}

}
