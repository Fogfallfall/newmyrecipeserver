package com.jinhyun.newmyrecipe.spring;

public interface RecipesDao
{
	public String selectRecipe(int menu_id);
	public void insertRecipe(Recipes recipes);
	public void updateRecipe(Recipes recipes);
	public void delete(int menu_id);
}
