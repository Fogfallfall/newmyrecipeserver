package com.jinhyun.newmyrecipe.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jinhyun.newmyrecipe.spring.Categories;
import com.jinhyun.newmyrecipe.spring.CategoriesDao;

@Service
public class CategoriesServiceImpl implements CategoriesService
{
	@Autowired
	private CategoriesDao categoriesDao;
	
	@Override
	public List<Categories> selectAll() 
	{
		return categoriesDao.selectAll();
	}
	
	@Override
	public Categories selectCategory(int id)
	{
		return categoriesDao.selectCategory(id);
	}

	@Override
	public int count() 
	{
		return categoriesDao.count();
	}

	@Override
	public void insert(String category_name) 
	{
		categoriesDao.insert(category_name);
	}

	@Override
	public void update(Categories categories) 
	{
		categoriesDao.update(categories);
	}

	@Override
	public void delete(int id) 
	{
		categoriesDao.delete(id);
	}
	
}